<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     $data =[
         [
             'name' =>'Super Admin',
             'hint' =>'This role can access every admin panels.',
         ],
         [
             'name' =>'User Admin',
             'hint' =>'This role can access user related admin panels.',
         ],
         [
             'name' =>'Advertisement Admin',
             'hint' =>'This role can access Advertisement related admin panels.',
         ],
         
     ];

     foreach ($data as $datum){
         \App\Models\Role::create([
             'name' => $datum['name'],
             'slug' => str_slug($datum['name']),
             'hint' => $datum['hint'],
         ]);
     }



    }
}
