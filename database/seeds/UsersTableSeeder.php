<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name' => 'Super Admin',
            'email' => 'root@ecommerce.local',
            'password' =>bcrypt('root'),
        ]);

        $role =\App\Models\Role::where('slug', 'super-admin')->first();
        $user->roles()->attach($role->id, [
            'created_at' =>\Carbon\Carbon::now(),
            'updated_at' =>\Carbon\Carbon::now()
        ]);
    }


}
