<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Currency::create([
            'symbol' => '$',
            'title' => 'USD',
            'is_default' => 1,
            'rate' => 1,
            'status' => 1,
            'rank' => 1,

        ]);
    }
}
