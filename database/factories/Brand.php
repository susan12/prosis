<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Brand::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'logo' => $faker->imageUrl('250','150'),
        'description' => $faker->realText(500),
        'status' =>rand(0,1),
        'domain' => $faker->url
    ];
});
