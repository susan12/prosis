<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Video;

class VideoController extends BaseController
{
    protected $base_route = 'admin.video';
    protected $view_path = 'admin.video';
    protected $panel = 'Video';
    protected $folder;

    public function __construct(){
        $this->model = new Video();

        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'uploads/videos' . DIRECTORY_SEPARATOR . $this->folder;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $video = $this->model->all();

        return view(parent::loadDefaultDataToView($this->view_path.'.index'), [
            'video' => $video,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:40',
            'excerpt' => 'required|max:300',
            'descripton' => 'required|max:200',
            'video_cont'  => 'file|max:10500|mimes:mp4,avi',
            'links' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'status' => 'in:0,1',
        ]);

        $video = new Video();

            $image_name = null;
            if ($request->hasFile('file')) {

                $file = $request->file('file');
                $file_name = mt_rand(1001, 9999).'_'.$file->getClientOriginalName();
                $file->move($this->folder_path, $file_name);

            }

        $video->title = $request->input('title');
        $video->excerpt = $request->input('excerpt');
        $video->descripton = $request->input('descripton');
        $video->links = $request->input('links');
        $video->status = $request->input('status');
        $video->save();

        return redirect('admin/video')->with('success_message' , 'New Video has been Uploaded Successfully!!!');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $video = $this->model->find($id);
        return view(parent::loadDefaultDataToView($this->view_path.'.edit'),[
            'video' => $video
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::find($id);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destination = 'uploads/videos';
            $extension = $file->getClientOriginalExtension();
            $file_name = $destination . '/' . md5(time()) . '.' . $extension;

            if ($video->file && app('files')->exists($video->file)) {
                app('files')->delete($video->file);
            }

            $file->move($destination, $file_name);
            $video->file = $video_name;
        }
        $video->title = $request->input('title');
        $video->links= $request->input('links');
        $video->rank = $request->input('rank');
        $video->status = $request->input('status');

        $video->save();

        return  redirect()->route($this->base_route.'.index')->with('success_message', 'New Video has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function publish($id)
    {
        $video = Video::find($id);
        $video->status = $video->status ? 0 : 1;
        $video->save();
        return redirect()->back();;
    } 

     public function sortItem(Request $request)
     {
        $data = $request->input('data');
        foreach ($data as $key => $value) {
            $slider = Slider::find($value);
            $slider->sort = $key;
            $slider->save();
        }
        return response()->json(['message' => 'Change made successfully.'], 200);
     }

            
    public function destroy(Request $request, $id)
    {
        $slider = $this->model::find($id);

        if ($slider->image && app('files')->exists($slider->image)) {
            app('files')->delete($slider->image);
        }
        $slider->delete();
        return redirect()->back()->with('success_message', 'Slider has been successfully deleted.');
    }
}
