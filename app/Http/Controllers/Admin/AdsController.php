<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\Ads\AddFormValidation;
use App\Http\Requests\Admin\Ads\EditFormValidation;

use App\Models\Ads;

class AdsController extends BaseController
{
    protected $base_route = 'admin.ads';
    protected $view_path = 'admin.ads';
    protected $panel = 'Ads';
    protected $model;
    protected $folder = 'ads';
    protected $folder_path;

    public function __construct()
    {
        $this->model = new Ads();
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->all();

        return view(parent::loadDefaultDataToView($this->view_path . '.index'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultDataToView($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        $content = null;
        if ($request->get('ad_type') == 'script_ad') {
            $content = $request->get('content');
        } else {
            // upload image if exist
            $image_name = null;
            if ($request->hasFile('file')) {

                $file = $request->file('file');
                $image_name = mt_rand(1001, 9999) . '_' . $file->getClientOriginalName();
                $file->move($this->folder_path, $image_name);

            }
            $content = $image_name;
        }

        $this->model->create([
            'ad_type' => $request->get('ad_type'),
            'title' => $request->get('title'),
            'link' => $request->get('link'),
            'status' => $request->get('status'),
            'content' => $content
        ]);

        return redirect()->route($this->base_route . '.index');
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        if (!$data['row']) {
            $request->session()->flash('error_message', 'Invalid Request!!');
            return redirect()->route($this->base_route . '.index');
        }

        return view(parent::loadDefaultDataToView($this->view_path . '.edit'), compact('data'));
    }

    public function update(EditFormValidation $request, $id)
    {
        $row = $this->model->find($id);
        if (!$row) {
            $request->session()->flash('error_message', 'Invalid Request!!');
            return redirect()->route($this->base_route . '.index');
        }

        if ($request->get('ad_type') == 'script_ad') {

            // remove image if old was banner ad
            if ($row->ad_type == 'banner_ad' && $row->content) {
                $path = $this->folder_path . DIRECTORY_SEPARATOR . $row->content;
                if (file_exists($path))
                    unlink($path);
            }

            $row->content = $request->get('content');


        } else {

            // image processingl
            if ($request->hasFile('file')) {

                // upload image
                $image = $request->file('file');
                $bannername = mt_rand(1001, 9999) . '_' . $image->getClientOriginalName();
                $image->move($this->folder_path, $bannername);

                // remove old image
                if ($row->ad_type == 'banner_ad' && $row->content) {
                    $path = $this->folder_path . DIRECTORY_SEPARATOR . $row->content;
                    if (file_exists($path))
                        unlink($path);
                }

                $row->content = $bannername;

            }

        }

        $row->update([
            'ad_type' => $request->get('ad_type'),
            'title' => $request->get('title'),
            'status' => $request->get('status'),
            'link' => $request->get('link'),
        ]);

        $request->session()->flash('success_message', $this->panel . ' updated successfully.');
        return redirect()->route($this->base_route . '.index');
    }

    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);



        // remove image
        if ($row->content) {
            $file_path = $this->folder_path . DIRECTORY_SEPARATOR . $row->content;
            if (file_exists($file_path)) {
                unlink($file_path);
            }
        }

        $row->delete();

        $request->session()->flash('success_message', $this->panel . ' deleted successfully.');
        return redirect()->route($this->base_route . '.index');
    }


}
