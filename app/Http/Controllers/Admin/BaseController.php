<?php
/**
 * Created by PhpStorm.
 * User: SK
 * Date: 5/26/2018
 * Time: 9:22 AM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use View;

class BaseController extends Controller
{
    /**
     * Get view path and loaded default params to it
     *through view composer
     * @param $view_path
     * @return mixed
     */
    public function loadDefaultDataToView($view_path)
    {
        //dd($view_path);
        //viewcomposer view_path to view as instance of object
        View::composer($view_path, function ($view){
            $view->with('panel',$this->panel);
            $view->with('base_route',$this->base_route);
            $view->with('view_path',$this->view_path);
            });
        return $view_path;
    }

    /**
     * Redirect to base route if row do not exist
     *
     * @param $row Model instance
     * @return $this
     */
    protected function redirectIfIsNull($row)
    {
        if (!$row) {
            request()->session()->flash('error_message', 'Invalid request.');
            return redirect()->route($this->base_route.'.index')->send();
        }
    }


    protected function redirectIfNotEditable($row)
    {
        if(!$row->isEditable()) {
            request()->session()->flash('error_message',' Invalid request.');
            return redirect()->route($this->base_route.'.index')->send();
        }
    }

    protected function redirectIfNotDeletable($row)
    {
        if(!$row->isDeletable()) {
//            dd('in');
            request()->session()->flash('error_message',' Invalid request.');
            return redirect()->route($this->base_route.'.index')->send();
        }
    }

    protected function redirectRequest($id = null)
    {
        if (request()->has('submit-continue')) {
            if($id){
                return redirect()->route($this->base_route.'.edit',$id);

            }else
            return redirect()->route($this->base_route.'.create');
        }else{
            return redirect()->route($this->base_route.'.index');
        }
    }
}