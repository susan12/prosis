<?php
/**
 * Created by PhpStorm.
 * User: SK
 * Date: 5/21/2018
 * Time: 7:50 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\Blogs\AddFormValidation;
use App\Http\Requests\Admin\Blogs\EditFormValidation;
use App\Models\Blog;
use Illuminate\Http\Request;


class BlogController extends BaseController
{
    protected $view_path = 'admin.blog';
    protected $base_route = 'admin.blogs';
    protected $folder = 'blogs';
    protected $folder_path;
    protected $panel ='Blog';

    public function __construct()
    {
        //dd(public_path());
        $this->model = new Blog();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder;
        //dd($this->folder_path);
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows']=$this->model
            ->select('id','created_at','updated_at','title')
            ->where(function ($query) use ($request) {
                //dd($request);

                if($request->has('id') && $request->get('id')) {
                    $query->where('id', $request->get('id'));
                }
                if($request->has('title') && $request->get('title')) {
                    $query->where('title','LIKE','%'.$request->get('title').'%');
                }


                if (
                    $request->has('created_at_start') && $request->get('created_at_start')
                    && $request->has('created_at_end') && $request->get('created_at_end')
                ) {
                    $query->whereBetween('created_at', [$request->get('created_at_start'), $request->get('created_at_end')] );
                }else{
                    if($request->has('created_at_start') && $request->get('created_at_start')){

                        $query->where('created_at', 'LIKE', $request->get('created_at_start').'%');
                    }
                    if($request->has('created_at_end') && $request->get('created_at_end')){

                        $query->where('created_at', 'LIKE', $request->get('created_at_end').'%');
                    }
                }

                if (
                    $request->has('updated_at_start') && $request->get('updated_at_start')
                    && $request->has('updated_at_end') && $request->get('updated_at_end')
                ) {
                    $query->whereBetween('updated_at', [$request->get('updated_at_start'), $request->get('updated_at_end')] );
                }else{
                    if($request->has('updated_at_start') && $request->get('updated_at_start')){

                        $query->where('updated_at', 'LIKE', $request->get('updated_at_end').'%');
                    }
                    if($request->has('updated_at_end') && $request->get('updated_at_end')){

                        $query->where('updated_at', 'LIKE', $request->get('updated_at_end').'%');
                    }
                }


            })
            ->get();
//        $data['view_path'] = $this->view_path; //to pass from controller to view for common part
//        $data['panel'] = $this->panel;
        return view(parent::loadDefaultDataToView($this->view_path.'.index'),compact('data'));
    }

    public function create()
    {

        $data['panel'] = $this->panel;
        return view(parent::loadDefaultDataToView($this->view_path.'.create'));
    }

    public function store(AddFormValidation $request)
    {
//      dd('blog controler');
//     dd($request->all());
        //Uploading image to folder
        $image_name = null;
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $image_name = mt_rand(0000,9999).$file->getClientOriginalName();
            $file->move($this->folder_path,$image_name);

        }

        Blog::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'status'=>$request->get('status'),
            'image'=>$image_name,
        ]);

        $request->session()->flash('success_message',$this->panel.' created Successfully.');
       return redirect()->route($this->base_route.'.index');
    }


    public function edit( Request $request, $id )
    {
       $data = [];
       $data['row'] = Blog::find($id);
      //dd($data);

        //for invalid request
        if(!$data['row'])
        {
            $request->session()->flash('error_message','Invalid Request');
            return redirect()->route('admin.blogs.index');
        }
//        $data['view_path'] = $this->view_path;
//        $data['panel'] = $this->panel;
       return view(parent::loadDefaultDataToView($this->view_path.'.edit'),compact('data'));

    }


    public  function update(EditFormValidation $request,$id)
    {
        //dd($id);
        //dd($request->all());
        $row = Blog::find($id);
        //for invalid request
        if(!$row)
        {
            $request->session()->flash('error_message','Invalid Request');
            return redirect()->route('admin.blogs.index');
        }

        //image processing
        if($request->hasFile('file'))
        {
            $old_image = $row->image;
            //upload image
            $image = $request->file('file');
            $row->image = mt_rand(1001,9999).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path, $row->image);

            //remove old image
            if($old_image)
            {
                $path = $this->folder_path.DIRECTORY_SEPARATOR.$old_image;
                if(file_exists($path))
                    unlink($path);
            }

        }

        $row->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'status'=>$request->get('status'),
            'image'=>$row->image,
        ]);
        $request->session()->flash('success_message',$this->panel.' Updated successfully. ');
        return redirect()->route($this->base_route.'.index');
    }

    public  function delete(Request $request, $id)
    {

        $row = Blog::find($id);

        //remove image

        if ($row->image){
            $file_path =$this->folder_path.DIRECTORY_SEPARATOR.$row->image;
            if(file_exists($file_path))
            {
                unlink($file_path);
            }
        }

        $row->delete();
        $request->session()->flash('success_message',$this->panel.' deleted Successfully.');
        return redirect()->route('admin.blogs.index');
    }



}