<?php
/**
 * Created by PhpStorm.
 * User: SK
 * Date: 5/21/2018
 * Time: 7:50 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Users\AddFormValidation;
use App\Http\Requests\Admin\Users\EditFormValidation;
use App\Http\Requests\Admin\Users\ProfileEditFormValidation;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;


class UserController extends BaseController
{
    protected $view_path = 'admin.users';
    protected $base_route = 'admin.users';
    protected $folder = 'Users';
    protected $folder_path;
    protected $panel ='User';

    public function __construct()
    {
        $this->model = new User();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index(Request $request)
    {

       // $this->authorize('view',new User());
        $data = [];
        $data['rows']=$this->model
            ->select('id','created_at','updated_at','name','email')
            ->where(function ($query) use ($request) {
                //dd($request);

                if($request->has('id') && $request->get('id')) {
                    $query->where('id', $request->get('id'));
                }
                if($request->has('name') && $request->get('name')) {
                    $query->where('name','LIKE','%'.$request->get('name').'%');
                }
                if($request->has('email') && $request->get('email')) {
                    $query->where('email','LIKE','%'.$request->get('email').'%');
                }

               if (
                   $request->has('created_at_start') && $request->get('created_at_start')
                   && $request->has('created_at_end') && $request->get('created_at_end')
               ) {
                   $query->whereBetween('created_at', [$request->get('created_at_start'), $request->get('created_at_end')] );
               }else{
                    if($request->has('created_at_start') && $request->get('created_at_start')){

                   $query->where('created_at', 'LIKE', $request->get('created_at_start').'%');
                    }
                   if($request->has('created_at_end') && $request->get('created_at_end')){

                       $query->where('created_at', 'LIKE', $request->get('created_at_end').'%');
                   }
               }

                if (
                    $request->has('updated_at_start') && $request->get('updated_at_start')
                    && $request->has('updated_at_end') && $request->get('updated_at_end')
                ) {
                    $query->whereBetween('updated_at', [$request->get('updated_at_start'), $request->get('updated_at_end')] );
                }else{
                    if($request->has('updated_at_start') && $request->get('updated_at_start')){

                        $query->where('updated_at', 'LIKE', $request->get('updated_at_end').'%');
                    }
                    if($request->has('updated_at_end') && $request->get('updated_at_end')){

                        $query->where('updated_at', 'LIKE', $request->get('updated_at_end').'%');
                    }
                }


            })
            ->get();

        return view(parent::loadDefaultDataToView($this->view_path.'.index'),compact('data'));
    }

    public function create()
    {
        // $this->authorize('create',new User());
        $data = [];
        $data['roles'] = Role::select('id','name')->orderBy('id')->get();
        $data['panel'] = $this->panel;
        return view(parent::loadDefaultDataToView($this->view_path.'.create'), compact('data'));
    }

    public function store(AddFormValidation $request)
    {


        $request->merge(['password' => bcrypt($request->get('password'))]);
        $user = $this->model->create($request->all());

        $data =[];
        foreach ($request->get('roles')as $role_id) {
            $data[$role_id] = [
                'created_at' =>\Carbon\Carbon::now(),
                'updated_at' =>\Carbon\Carbon::now()
            ];
        }
        $user->roles()->sync($data);
        $request->session()->flash('success_message',$this->panel.' created Successfully.');
        return parent::redirectRequest();

    }

    public function show( Request $request, $id)
    {

        $data = [];
        $data['row'] =$this->model->find($id);

       parent::redirectIfIsNull($data['row']);
        return view(parent::loadDefaultDataToView($this->view_path.'.show'),compact('data'));
    }

    public function edit(Request $request, $id )
    {
       $data = [];
       $data['row'] = $this->model->find($id);
        parent::redirectIfIsNull($data['row']);
        parent::redirectIfNotDeletable($data['row']);

        $data['user_roles'] = $data['row']->roles()->pluck('roles.name','roles.id')->toArray();
        $data['roles'] = Role::select('id','name')->orderBy('id')->get();

       return view(parent::loadDefaultDataToView($this->view_path.'.edit'),compact('data'));

    }


    public  function update(EditFormValidation $request,$id)
    {
 
        $row = $this->model->find($id);

        parent::redirectIfIsNull($row);


        if($request->get('password'))
        {
            $request->merge(['password' => bcrypt($request->get('password'))]);

        }else
        {
            $request->merge(['password' => $row->password]);
        }

        $data =[];
        foreach ($request->get('roles')as $role_id) {
            $data[$role_id] = [
                'created_at' =>\Carbon\Carbon::now(),
                'updated_at' =>\Carbon\Carbon::now()
            ];
        }

        $row->roles()->sync($data);

        $row->update($request->all());
        $request->session()->flash('success_message',$this->panel.' Updated successfully. ');
        return parent::redirectRequest($id);
    }

    public  function destroy(Request $request, $id)
    {

        $row = $this->model->find($id);
//        dd($row);
        parent::redirectIfIsNull($row);
        parent::redirectIfNotDeletable($row);
        $row->roles()->sync([]);
        $row->delete();
        $request->session()->flash('success_message',$this->panel.' deleted Successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function profileEdit()
    {

        $data = [];
        $data['row'] = auth()->user();
        return view(parent::loadDefaultDataToView($this->view_path.'.profile'),compact('data'));

    }

    public function profileUpdate(ProfileEditFormValidation $request)
    {

        $row = auth()->user();

        parent::redirectIfIsNull($row);


        if($request->get('password'))
        {
            $request->merge(['password' => bcrypt($request->get('password'))]);

        }else
        {
            $request->merge(['password' => $row->password]);
        }
        

        $row->update($request->all());
        $request->session()->flash('success_message',$this->panel.' Updated successfully. ');
        return redirect()->back();

    }




}