<?php
/**
 * Created by PhpStorm.
 * User: SK
 * Date: 5/21/2018
 * Time: 7:50 PM
 */

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use Illuminate\Http\Request;


class RoleController extends BaseController
{
    protected $base_route = 'admin.roles';
    protected $view_path = 'admin.roles';
    protected $panel = 'Roles';
    protected $model;
    protected $folder = 'roles';
    protected $folder_path;

    public function __construct()
    {
        //dd(public_path());
        $this->model = new Role();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder;
        //dd($this->folder_path);
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows']=$this->model
            ->select('id','created_at','updated_at','name','slug','hint')
            ->paginate();
//        $data['view_path'] = $this->view_path; //to pass from controller to view for common part
//        $data['panel'] = $this->panel;
        return view(parent::loadDefaultDataToView($this->view_path.'.index'),compact('data'));
    }

    public function show( Request $request, $id)
    {

        $data = [];
        $data['row'] =$this->model->find($id);
        parent::redirectIfIsNull($data['row']);

        return view(parent::loadDefaultDataToView($this->view_path.'.show'),compact('data'));
    }

    public function edit(Request $request, $id )
    {
       $data = [];
       $data['row'] = $this->model->find($id);
        parent::redirectIfIsNull($data['row']);
      //dd($data);

//        $data['view_path'] = $this->view_path;
//        $data['panel'] = $this->panel;
       return view(parent::loadDefaultDataToView($this->view_path.'.edit'),compact('data'));

    }

    public  function update(Request $request,$id)
    {
        //dd($id);
//        dd($request->all());
//        $row = User::find($id);
        //for invalid request
        $row = $this->model->find($id);
        parent::redirectIfIsNull($row);
        $data = $request->except(['slug']);
        $row->update($data);

        $request->session()->flash('success_message',$this->panel.' Updated successfully. ');
        return parent::redirectRequest($id);
    }




}