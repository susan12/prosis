<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Slider;

class SliderController extends BaseController
{
    protected $base_route = 'admin.slider';
    protected $view_path = 'admin.slider';
    protected $model;
    protected $panel = 'Slider';
    protected $folder;

    public function __construct(){
        $this->model = new Slider();
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'uploads/slider' . DIRECTORY_SEPARATOR . $this->folder;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $slider = $this->model->orderBy('sort','ASC')->get();
        

        return view(parent::loadDefaultDataToView($this->view_path.'.index'), [
            'slider' => $slider,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'title' => 'required|max:40',
            'caption' => 'required|max:200',
            'links' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'image' => 'required|mimes:jpg,jpeg,png,gif',
        ]);

        $image_name = null;
        if ($request->hasfile('image')) {
            $file =$request->file('image');
            $image_name = mt_rand(1001, 9999).'_'.$file->getClientOriginalName();
            $file->move($this->folder_path, $image_name);
        }

        $this->model->create([
            'title' => $request->get('title'),
            'links' => $request->get('links'),
            'caption' => $request->get('caption'),
            'status' => $request->get('status'),
            'rank' => 0,
            'image' => $image_name
        ]);

        return redirect('admin/slider')->with('success_message' , 'New Slider has been added Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $slider = $this->model->find($id);
        parent::redirectIfIsNull($slider);
        return view(parent::loadDefaultDataToView($this->view_path.'.edit'),[
            'slider' => $slider
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = $this->model->find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $destination = 'images/slider';
            $extension = $image->getClientOriginalExtension();
            $image_name = $destination . '/' . md5(time()) . '.' . $extension;

            if ($slider->image && app('files')->exists($slider->image)) {
                app('files')->delete($slider->image);
            }

            $image->move($destination, $image_name);
            $slider->image = $image_name;
        }
        $slider->title = $request->input('title');
        $slider->links= $request->input('links');
        $slider->caption = $request->input('caption');
        $slider->rank = $request->input('rank');
        $slider->status = $request->input('status');

        $slider->save();

        return  redirect()->route($this->base_route.'.index')->with('success_message', 'New slider has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function publish($id)
    {
        $slider = $this->model->find($id);
        $slider->status = $slider->status ? 0 : 1;
        $slider->save();
        return redirect()->back();
    } 

     public function sortItem(Request $request)
      {
        $data = $request->input('data');
        foreach ($data as $key => $value) {
            $slider = Slider::find($value);
            $slider->sort = $key;
            $slider->save();
        }
        
        return response()->json(['message' => 'Change made successfully.'], 200);
    }

    public function destroy(Request $request, $id)
    {
        $slider = $this->model::find($id);

        if ($slider->image && app('files')->exists($slider->image)) {
            app('files')->delete($slider->image);
        }
        $slider->delete();
        return redirect()->back()->with('success_message', 'Slider has been successfully deleted.');
    }
}
