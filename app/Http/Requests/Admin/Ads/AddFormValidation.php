<?php

namespace App\Http\Requests\Admin\Ads;

use Illuminate\Foundation\Http\FormRequest;

class AddFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required | max:100',
            'status' => 'in:1,0',
            'ad_type' => 'required | in:banner_ad,script_ad'
        ];

        if ($this->request->get('ad_type') == 'script_ad') {
            $rules = $rules + [
                    'content' => 'required'
                ];
        } else {
            $rules = $rules + [
                    'file' => 'required | image'

                ];
        }

        return $rules;
    }
}
