<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;



class AddFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd($this->request->all());
        $this->customValidations();
        return [
            'name' => 'required | max:255 |unique:users,name',
            'email' => 'required | max:50 | email | unique:users,email',
            'password' => 'required | min:6 | confirmed',
            'roles' => 'required | role_selected',
        ];
    }

    public function customValidations()
    {
        Validator::extend('role_selected', function ($attribute, $value, $parameters, $validator) {
//            dd($value);
            return is_array($value);
        });
    }
}
