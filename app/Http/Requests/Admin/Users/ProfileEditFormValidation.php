<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


class ProfileEditFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      //dd($this->request->all());
        $rules = [
            'name' => 'required | max:255 | unique:users,name,'.$this->request->get('id'),
            'email' => 'required | max:50 | email | unique:users,email,'.$this->request->get('id'),
        ];
        if (!$this->request->get('password'))
        {
            return $rules;
        }else
        {
            $rules = $rules + [
                'password' => 'max:6 | confirmed'
                ];
            return $rules;
        }

    }

}
