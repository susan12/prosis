<?php

namespace App\Http\Requests\Admin\Brand;

use Illuminate\Foundation\Http\FormRequest;

class AddFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        dd($this->request->all());
        return [
            'file' => 'image',
            'title' => 'required | max:100 | unique:currencies,title',
            'status' => 'required | in:1,0',
            'rank' => 'required | numeric'
        ];
    }
}
