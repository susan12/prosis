<?php
/**
 * Created by PhpStorm.
 * User: SK
 * Date: 6/4/2018
 * Time: 6:04 AM
 */

namespace App\Policies;


class BasePolicy

{
    protected $roles;

    public function __construct()
    {
        $this->roles = auth()->user()->roles;
    }

    protected function hasAccess($access_roles)
    {
        if ($this->roles->count() > 0) {
            foreach ($this->roles as $role) {
                if (in_array($role->slug, $access_roles))
                    return true;
            }

            return false;

        }else
        {
            return false;
        }
    }
}