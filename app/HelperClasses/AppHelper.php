<?php
/**
 * Created by PhpStorm.
 * User: SK
 * Date: 5/28/2018
 * Time: 7:47 PM
 */

namespace App\HelperClasses;


class AppHelper
{
    public static function convertDate($date, $format='jS M, Y H:i A')
        
    {
        return date($format, strtotime($date));

    }
}