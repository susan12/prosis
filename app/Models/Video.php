<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Html;

class Video extends Model
{
	protected $fillable = ['title','excerpt','description','video_cont','links','rank','status'];
	
	public function renderVideo(array $option =null) {
	    return Html::image($this->pathToImage($this->video_cont),$this->name,$option);
	}

	public function pathToImage($path) {
	    if(filter_var($path,FILTER_VALIDATE_URL)){
	        $path_ = $path;
	    }
	    else{
	        $path_ = $path?url('/').'/'.$path:"";
	    }
	    return $path_;
	}

	public function publish(){
        if($this->status ==1){
            return '<a href="'.route('admin.video.publish',$this->id).'"> <span class="btn btn-circle btn-success">Active</span></a>';
        }else{
            return '<a href="'.route('admin.video.publish',$this->id).'"> <span class="btn btn-circle btn-danger">Inactive</span></a>';
        }
    }
	public function renderImage(array $option =null) {
	    return Html::image($this->pathToImage($this->image),$this->name,$option);
	}

}
