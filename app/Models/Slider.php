<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Html;

class Slider extends Model
{
	protected $table = 'sliders';
	protected $fillable = ['title','image','links','caption','rank','sort','status'];
	
	public function pathToImage($path) {
	    if(filter_var($path,FILTER_VALIDATE_URL)){
	        $path_ = $path;
	    }
	    else{
	        $path_ = $path?url('/').'/'.$path:"";
	    }
	    return $path_;
	}

	public function publish(){
        if($this->status ==1){
            return '<a href="'.route('admin.slider.publish',$this->id).'"> <span class="btn btn-circle btn-success">Active</span></a>';
        }else{
            return '<a href="'.route('admin.slider.publish',$this->id).'"> <span class="btn btn-circle btn-danger">Inactive</span></a>';
        }
    }

    public function renderImage()
    {
    	 return Html::image($this->pathToImage($this->image),$this->name,$option);
    }
	
}
