<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'slug', 'hint'];

    /**
     * The roles that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany(Role::class);
    }
}


