<?php

//dd(bcrypt('root'));
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('show-table',['uses'=>'TestController@showTable']);

Route::group(['prefix' => 'admin/', 'as' => 'admin.', 'namespace' => 'Admin\\','middleware' => 'auth'], function () {

    Route::get('dashboard',             ['as' => 'dashboard',           'uses' => 'DashboardController@index']);
    Route::get('test',                  ['as' => 'test',                'uses' => 'DashboardController@test']);

    //Route::resource('ads', 'AdsController');
    Route::get('ads',                   ['as' => 'ads.index',          'uses' => 'AdsController@index']);
    Route::get('ads/create',            ['as' => 'ads.create',         'uses' => 'AdsController@create']);
    Route::post('ads',                  ['as' => 'ads.store',          'uses' => 'AdsController@store']);
    Route::get('ads/{id}/edit',         ['as' => 'ads.edit',           'uses' => 'AdsController@edit']);
    Route::put('ads/{id}',              ['as' => 'ads.update',         'uses' => 'AdsController@update']);
    Route::delete('ads/{id}',           ['as' => 'ads.destroy',        'uses' => 'AdsController@destroy']);
    Route::delete('slider/{id}',           ['as' => 'slider.destroy',        'uses' => 'SliderController@destroy']);

    Route::resource('video','VideoController');
    Route::get('video/publish/{id}', 'VideoController@publish')->name('video.publish');
    Route::get('blogs',['as'=>'blogs.index','uses'=>'BlogController@index']);
    Route::get('blogs/create',['as'=>'blogs.create','uses'=>'BlogController@create']);
    Route::post('blogs/store',['as'=>'blogs.store','uses'=>'BlogController@store']);
    Route::get('blogs/{id}/edit',['as'=>'blogs.edit','uses'=>'BlogController@edit']);
    Route::post('blogs/{id}/update',['as'=>'blogs.update','uses'=>'BlogController@update']);
    Route::get('blogs/{id}/delete',['as'=>'blogs.delete','uses'=>'BlogController@delete']);


    Route::get('users',                 ['as' => 'users.index',         'uses' => 'UserController@index']);
    Route::get('users/create',          ['as' => 'users.create',        'uses' => 'UserController@create']);
    Route::post('users',                ['as' => 'users.store',         'uses' => 'UserController@store']);
    Route::get('users/{id}',            ['as' => 'users.show',          'uses' => 'UserController@show']);
    Route::get('users/{id}/edit',       ['as' => 'users.edit',          'uses' => 'UserController@edit']);
    Route::put('users/{id}',            ['as' => 'users.update',        'uses' => 'UserController@update']);
    Route::delete('users/{id}',         ['as' => 'users.destroy',        'uses' => 'UserController@destroy']);
    Route::get('users/profile/update',  ['as' => 'users.profile.edit', 'uses' => 'UserController@profileEdit']);
    Route::put('users/profile/update',  ['as' => 'users.profile.update', 'uses' => 'UserController@profileUpdate']);

    Route::get('roles',                 ['as' => 'roles.index',         'uses' => 'RoleController@index']);
    Route::get('roles/{id}',            ['as' => 'roles.show',          'uses' => 'RoleController@show']);
    Route::get('roles/{id}/edit',       ['as' => 'roles.edit',          'uses' => 'RoleController@edit']);
    Route::put('roles/{id}',            ['as' => 'roles.update',        'uses' => 'RoleController@update']);

   Route::resource('slider','SliderController');
   Route::get('slider/publish/{id}', 'SliderController@publish')->name('slider.publish');


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
