


<div class="form-group">
    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    <div class="col-sm-9">
        {!! Form::text('name',null,['class'=>'col-xs-10 col-sm-5','placeholder'=>'Role Name']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('slug', 'Slug', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    <div class="col-sm-9">
        {!! Form::text('slug',null,['class'=>'col-xs-10 col-sm-5','placeholder'=>'Slug','disabled']) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('hint', 'Hint', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    <div class="col-sm-9">
        {!! Form::text('hint',null,['class'=>'col-xs-10 col-sm-5','placeholder'=>'Hint']) !!}
    </div>
</div>

<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">

        <button class="btn btn-info" type="submit" name="">
            <i class="icon-ok bigger-110"></i>
            Submit
        </button>



        <button class="btn" type="reset">
            <i class="icon-undo bigger-110"></i>
            Reset
        </button>

    </div>
</div>

