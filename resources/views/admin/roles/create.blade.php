@extends('admin.includes.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            @include($view_path.'.includes.breadcrumb',[
            'action' => 'Add Form',
            'panel' => $panel,
            ])

        </div>

        <div class="page-content">
             @include($view_path.'.includes.breadcrumb_sub',[
             'action' => 'Create',
             ])

            <div class="row">
                <div class="col-xs-12">
                    @include('admin.includes.form_validation_messages')

                <!-- PAGE CONTENT BEGINS-->

                    {!! Form::open([
                    'url' => route($base_route.'.create'),
                    'class'=>'form-horizontal',
                    'role'=>'form',
                    'enctype'=>'multipart/form-data',
                    ]) !!}


                    @include($view_path.'.includes.form')

                    {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection


