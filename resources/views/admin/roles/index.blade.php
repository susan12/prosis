@extends('admin.includes.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>


           @include('admin.includes.breadcrumb', [
           'action' => 'List Form',
           'panel' => $panel,
           ])

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            @include('admin.includes.breadcrumb_sub',[
            'action' => 'List Form',
            'panel' => $panel,
            ])

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                                @include('admin.includes.flash_messages')

                                <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Name</th>
                                        <th class="hidden-480">Slug</th>
                                        <th class="hidden-480">Hint</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if( $data['rows']->count() > 0 )

                                        @foreach($data['rows'] as $row)

                                            <tr>

                                                <td>
                                                    <a href="#">{{ $row->id }}</a>
                                                </td>
                                                <td class="hidden-480">{{ AppHelper::convertDate($row->created_at) }}</td>
                                                <td>{{ AppHelper::convertDate($row->updated_at) }}</td>
                                                <td>{{ $row->name }}</td>
                                                <td>{{ $row->slug }}</td>
                                                <td>{{ $row->hint }}</td>


                                                <td>
                                                    <div class="btn-group">

                                                        <a href="{{ route($base_route.'.show',$row->id) }}"
                                                           class="btn btn-xs btn-success">
                                                            <i class="icon-eye-open bigger-120"></i>
                                                        </a>


                                                        <a href="{{ route($base_route.'.edit',$row->id) }}"
                                                           class="btn btn-xs btn-info">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>


                                                    </div>

                                                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                                                        <div class="inline position-relative">
                                                            <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-cog icon-only bigger-110"></i>
                                                            </button>

                                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                <li>
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																				<span class="green">
																					<i class="icon-edit bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#" class="tooltip-error " data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach

                                        <tr>
                                            <td colspan="8">{!! $data['rows']->links() !!}</td>
                                        </tr>

                                    @else
                                        <tr>
                                            <td colspan="8">No data found..</td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>


                        </div><!-- /span -->
                    </div><!-- /row -->

                    <div class="hr hr-18 dotted hr-double"></div>




                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->


@endsection

