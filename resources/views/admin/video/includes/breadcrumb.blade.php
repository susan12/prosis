<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="{{ route('admin.dashboard') }}">Home</a>
    </li>

    <li>
        <a href="{{ route('admin.ads.index') }}">{{ $panel }}</a>
    </li>
    <li class="active">{{ $action  }}</li>
</ul><!-- .breadcrumb -->