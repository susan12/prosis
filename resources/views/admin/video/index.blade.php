@extends('admin.includes.layout')


@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            @include($view_path.'.includes.breadcrumb', [
                 'panel' => $panel,
                 'action' => 'Add Form'
             ])

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input"
                                           id="nav-search-input" autocomplete="off"/>
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            @include($view_path.'.includes.breadcrumb_sub', [
               'panel' => $panel,
               'action' => 'List'
           ])

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">

                            @if (session()->has('success_message'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">
                                        <i class="icon-remove"></i>
                                    </button>
                                    {!! session()->get('success_message') !!}
                                    <br>
                                </div>
                            @endif

                            @if (session()->has('error_message'))
                                <div class="alert alert-warning">
                                    <button type="button" class="close" data-dismiss="alert">
                                        <i class="icon-remove"></i>
                                    </button>
                                    {!! session()->get('error_message') !!}
                                    <br>
                                </div>
                            @endif

                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            <label>
                                                <input type="checkbox" class="ace"/>
                                                <span class="lbl"></span>
                                            </label>
                                        </th>
                                        <th>ID</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Title</th>
                                        <th>Video</th>
                                        <th>Short Description</th>
                                        <th>Description</th>
                                        <th>Link</th>
                                        <th>Rank</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if ($video->count() > 0)

                                        @foreach($video as $row)

                                            <tr>
                                                <td class="center">
                                                    <label>
                                                        <input type="checkbox" class="ace"/>
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <a href="#">{{ $row->id }}</a>
                                                </td>
                                                <td>{{ date('jS M, Y H:i A', strtotime($row->created_at)) }}</td>
                                                <td>{{ date('jS M, Y H:i A', strtotime($row->updated_at)) }}</td>
                                                <td>{{ $row->title }}</td>
                                                <td>
                                                    <video width="100" height="100" controls preload="none">
                                                        <source src="{{url($row->video_cont)}}" type="video/mp4">
                                                    </video>
                                                </td>
                                                <td>{{ $row->excerpt }}</td>
                                                <td>{!! $row->descripton !!}</td>
                                                <td>{{ $row->links }}</td>
                                                <td>{{ $row->rank }}</td>
                                                <td>{!! $row->publish() !!}
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a href="{{ route($base_route.'.edit', $row->id) }}"
                                                           class="btn btn-xs btn-info">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>
                                                        <a href="{{ route($base_route.'.destroy', $row->id) }}"
                                                           class="btn btn-xs btn-danger bootbox-confirm">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'url' => route($base_route.'.destroy', $row->id)
                                                            ]) !!}
                                                        @method('delete')
                                                        {!! Form::close() !!}

                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="11">No data.</td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                    <div class="hr hr-18 dotted hr-double"></div>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection

@section('extra_script_lib')
<script src="{{ asset('assets/admin-panel/js/bootbox.min.js') }}"></script>
@endsection

@section('extra-scripts')
    <script>
        
        $(".bootbox-confirm").on('click', function () {
            var $this = $(this);
            bootbox.confirm("Are you sure?", function (result) {
                if (result) {
                    $this.closest('div').find('form').submit();
                }
            });
            return false;
        });
    </script>
@endsection