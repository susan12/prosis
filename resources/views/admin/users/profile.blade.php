@extends('admin.includes.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            @include('admin.includes.breadcrumb' ,[
            'action' => 'Profile Form',
            'panel' => $panel,
            ])

        </div>

        <div class="page-content">
            @include('admin.includes.breadcrumb_sub',[
            'action' => 'Profile Form',
            ])

            <div class="row">
                <div class="col-xs-12">
                    @include('admin.includes.flash_messages')
                    @include('admin.includes.form_validation_messages')

                         {!! Form::model($data['row'],[
                            'url' => route($base_route.'.profile.update',$data['row']->id),
                            'class'=>'form-horizontal',
                            'role'=>'form',
                            'enctype'=>'multipart/form-data',
                                        ])
                          !!}

                        @method('PUT')

                        {!! Form::hidden('id',$data['row']->id) !!}

                  @include($view_path.'.includes.profile_form')
                        {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection


