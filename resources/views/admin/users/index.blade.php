@extends('admin.includes.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>


           @include('admin.includes.breadcrumb', [
           'action' => 'List Form',
           'panel' => $panel,
           ])

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            @include('admin.includes.breadcrumb_sub',[
            'action' => 'List Form',
            'panel' => $panel,
            ])

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">

                            @if(session()->has('success_message'))
                            <div class="alert alert-block alert-success">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="icon-remove"></i>
                                </button>
                            {!! session()->get('success_message') !!}
                            </div>
                            @endif


                                @if(session()->has('error_message'))
                                    <div class="alert alert-block alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! session()->get('error_message') !!}
                                    </div>
                                @endif

                                <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            <label>
                                                <input type="checkbox" class="ace"/>
                                                <span class="lbl"></span>
                                            </label>
                                        </th>
                                        <th width="5%">ID</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th class="hidden-480">Name</th>
                                        <th class="hidden-480">Email</th>
                                        <th></th>
                                    </tr>

                                    <tr>
                                        {!! Form::open(['method' => 'GET', 'url' => route($base_route.'.index')]) !!}
                                        <th class="center"></th>
                                        <th>{!! Form::text('id', null, ['class' => 'form-control']) !!}</th>
                                        <th>
                                            {!! Form::date('created_at_start', null, ['class' => 'form-control']) !!}
                                            {!! Form::date('created_at_end', null, ['class' => 'form-control']) !!}

                                        </th>
                                        <th>
                                            {!! Form::date('updated_at_start', null, ['class' => 'form-control']) !!}
                                            {!! Form::date('updated_at_end', null, ['class' => 'form-control']) !!}
                                        </th>
                                        <th>{!! Form::text('name', null, ['class' => 'form-control']) !!}</th>
                                        <th>{!! Form::text('email', null, ['class' => 'form-control']) !!}</th>
                                        <th>
                                            <button type="submit" href="#"
                                                    class="btn btn-xs btn-primary">
                                                <i class="icon-filter bigger-120"></i>
                                            </button>
                                        </th>
                                        {!! Form::close() !!}
                                    </tr>

                                    </thead>

                                    <tbody>

                                    @if( $data['rows']->count() > 0 )

                                        @foreach($data['rows'] as $row)

                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#">{{ $row->id }}</a>
                                                </td>
                                                <td class="hidden-480">{{ date('jS M, Y H:i A',strtotime($row->created_at )) }}</td>
                                                <td>{{ date('jS M, Y H:i A',strtotime($row->updated_at )) }}</td>
                                                <td>{{ $row->name }}</td>
                                                <td>{{ $row->email }}</td>


                                                <td>
                                                    <div class="btn-group">

                                                        <a href="{{ route($base_route.'.show',$row->id) }}"
                                                           class="btn btn-xs btn-success">
                                                            <i class="icon-eye-open bigger-120"></i>
                                                        </a>

                                                        @if($row->isEditable())
                                                        <a href="{{ route($base_route.'.edit',$row->id) }}"
                                                           class="btn btn-xs btn-info">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>
                                                        @endif

                                                        @if ($row->isDeletable())
                                                        <a href="{{ route($base_route.'.destroy',$row->id) }}"
                                                           class="btn btn-xs btn-danger bootbox-confirm">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>

                                                        {!! Form::open([
                                                                'url' =>route($base_route.'.destroy',$row->id)
                                                        ]) !!}
                                                            @method('delete')
                                                            {!! Form::close() !!}
                                                        @endif

                                                    </div>

                                                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                                                        <div class="inline position-relative">
                                                            <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-cog icon-only bigger-110"></i>
                                                            </button>

                                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                <li>
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																				<span class="green">
																					<i class="icon-edit bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#" class="tooltip-error " data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="7">No data found..</td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>


                        </div><!-- /span -->
                    </div><!-- /row -->

                    <div class="hr hr-18 dotted hr-double"></div>




                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->


@endsection

@section('extra_script_lib')
            <script src="{{ asset('assets/admin-panel/js/bootbox.min.js') }}"></script>

@endsection

        @section('extra-scripts')
            <script>
                $(".bootbox-confirm").on('click', function () {
                    var $this = $(this);
                    bootbox.confirm("Are you sure?", function (result) {
                        if (result) {
                           $this.closest('div').find('form').submit();
                        }
                    });
                    return false;
                });
            </script>
@endsection