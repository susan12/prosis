@extends('admin.includes.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            @include('admin.includes.breadcrumb',[
            'action' => 'Add Form',
            'panel' => $panel,
            ])

        </div>

        <div class="page-content">
             @include('admin.includes.breadcrumb_sub',[
             'action' => 'Create',
             ])

            <div class="row">
                <div class="col-xs-12">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                <!-- PAGE CONTENT BEGINS-->

                    {!! Form::open([
                    'url' => route($base_route.'.store'),
                    'class'=>'form-horizontal',
                    'role'=>'form',
                    'enctype'=>'multipart/form-data',
                    ]) !!}


                    @include($view_path.'.includes.form')

                    {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection


