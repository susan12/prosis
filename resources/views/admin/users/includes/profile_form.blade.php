


<div class="form-group">
    {!! Form::label('name', 'User Name', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    <div class="col-sm-9">
        {!! Form::text('name',null,['class'=>'col-xs-10 col-sm-5','placeholder'=>'User Name']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    <div class="col-sm-9">
        {!! Form::text('email',null,['class'=>'col-xs-10 col-sm-5','placeholder'=>'Email']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('password', isset($data['row'])?'New Password':'Password', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    <div class="col-sm-9">
        {!! Form::password('password', null, ['class'=>'col-xs-10 col-sm-5']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('password_confirmation', isset($data['row'])?'Confirm New Password':'Confirm Password', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    <div class="col-sm-9">
        {!! Form::password('password_confirmation', null, ['class'=>'col-xs-10 col-sm-5']) !!}
    </div>
</div>



<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">

        <button class="btn btn-info" type="submit" name="submit" >
            <i class="icon-ok bigger-110"></i>
            Submit
        </button>

        <button class="btn btn-info" type="submit" name="submit-continue">
            <i class="icon-ok bigger-110"></i>
            Submit & Continue
        </button>
        &nbsp; &nbsp; &nbsp;
        <button class="btn" type="reset">
            <i class="icon-undo bigger-110"></i>
            Reset
        </button>

    </div>
</div>

