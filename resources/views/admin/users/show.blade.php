@extends('admin.includes.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>


           @include('admin.includes.breadcrumb', [
           'action' => 'List Form',
           'panel' => $panel,
           ])

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            @include('admin.includes.breadcrumb_sub',[
            'action' => 'List Form',
            'panel' => $panel,
            ])

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">

                            @if(session()->has('success_message'))
                            <div class="alert alert-block alert-success">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="icon-remove"></i>
                                </button>
                            {!! session()->get('success_message') !!}
                            </div>
                            @endif


                                @if(session()->has('error_message'))
                                    <div class="alert alert-block alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! session()->get('error_message') !!}
                                    </div>
                                @endif

                                <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>

                                        <th width="25%">Column Name</th>
                                        <th>Data</th>
                                    </tr>
                                    </thead>

                                    <tbody>



                                            <tr>
                                                <td class="hidden-480">id</td>
                                                <td>{{ $data['row']->id }}</td>
                                            </tr>

                                            <tr>
                                                <td class="hidden-480">Name</td>
                                                <td>{{ $data['row']->name }}</td>
                                            </tr>

                                            <tr>
                                                <td class="hidden-480">Email</td>
                                                <td>{{ $data['row']->email }}</td>
                                            </tr>

                                            <tr>
                                                <td class="hidden-480">Created At</td>
                                                <td>{{ AppHelper::convertDate($data['row']->created_at) }}</td>
                                            </tr>

                                            <tr>
                                                <td class="hidden-480">Updated At</td>
                                                <td>{{ AppHelper::convertDate($data['row']->updated_at)  }}</td>
                                            </tr>
                                            @if($data['row']->isEditable())
                                            <tr>
                                                <td class="hidden-480">Edit</td>
                                                <td> <a href="{{ route($base_route.'.edit',$data['row']->id) }}"
                                                        class="btn btn-xs btn-info">
                                                        <i class="icon-edit bigger-120"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endif

                                    </tbody>
                                </table>


                        </div><!-- /span -->
                    </div><!-- /row -->

                    <div class="hr hr-18 dotted hr-double"></div>




                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->


@endsection
