@extends('admin.includes.layout')

@section('content')
    <style>
        .adtype {
            display: none;}
    </style>
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            @include($view_path.'.includes.breadcrumb', [
                 'panel' => $panel,
                 'action' => 'Add Form'
             ])
        </div>

        <div class="page-content">
            @include($view_path.'.includes.breadcrumb_sub', [
               'panel' => $panel,
               'action' => 'Edit Form'
           ])

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::model($data['row'], [
                        'url' => route($base_route.'.update', $data['row']->id),
                        'class' => 'form-horizontal',
                        'role' => 'form',
                        'enctype' => 'multipart/form-data'
                        ])

                        !!}
                    @method('PUT')

                    <div class="form-group">
                        {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('title', null, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Title']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('ad_type', 'Ad Type', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('ad_type', ['' => 'Select', 'banner_ad' => 'Banner Ad', 'script_ad' => 'Script Ad'], null, ['class' => 'col-xs-10 col-sm-5', 'id' => 'ad_type']) !!}
                        </div>
                    </div>
                    <div id="banner_ad_div" class="adtype">
                        <div class="form-group">
                            {!! Form::label('existing_image', 'Existing Image', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                            <div class="col-sm-9">
                                @if ($data['row']->content)
                                    <img src="{{ asset('images/ads/'.$data['row']->content) }}"
                                         style="max-width: 200px;" alt="">
                                @else
                                    <p>No Image</p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('file', 'Image', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                            <div class="col-sm-9">
                                {!! Form::file('file', ['class' => 'col-xs-10 col-sm-5']) !!}
                            </div>
                        </div>
                    </div>

                    <div id="script_ad_div" class="form-group adtype">

                        {!! Form::label('content', 'Script Ads', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::textarea('content', null, ['class' => 'col-xs-10 col-sm-5']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('link', 'Link', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('link', null, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Title']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], null, ['class' => 'col-xs-10 col-sm-5']) !!}
                        </div>
                    </div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Submit
                            </button>&nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection

@section('extra_script_lib')
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
@endsection

@section('extra-scripts')
    <script>
        $('.textarea').ckeditor();
    </script>
    <script type="text/javascript">

        $(function () {

            switchAdType($("#ad_type"));

            //$("#ad_type").trigger('change');
            $("#ad_type").change(function () {
                switchAdType($(this));
            });

        });

        function switchAdType($this) {
            var selectedVal = $this.val();

            $(".adtype").hide();

            if (selectedVal != '') {

                var divId = selectedVal + '_div';

                $("#" + divId).show();

            }
        }

    </script>
@endsection