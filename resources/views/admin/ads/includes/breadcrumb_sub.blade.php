<div class="page-header">
    <h1>
        {{ $panel }} Manager
        <small>
            <i class="icon-double-angle-right"></i>
            {{ $action }}
        </small>
    </h1>
</div><!-- /.page-header -->