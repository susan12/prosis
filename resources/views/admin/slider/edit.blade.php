@extends('admin.includes.layout')
@section('title')
Edit Slider
@endsection
@section('content')
    <style>
        .adtype {
            display: none;}
    </style>
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            @include($view_path.'.includes.breadcrumb', [
                 'panel' => $panel,
                 'action' => 'Edit Form'
             ])
        </div>

        <div class="page-content">
            @include($view_path.'.includes.breadcrumb_sub', [
               'panel' => $panel,
               'action' => 'Edit Form'
           ])

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::model($slider, [
                        'url' => route($base_route.'.update', $slider->id),
                        'class' => 'form-horizontal',
                        'role' => 'form',
                        'enctype' => 'multipart/form-data',
                        'method' => 'PUT'
                        ])

                        !!}

                    <div class="form-group">
                        {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('title', $slider->title, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Title']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('image','Photo',['class'=>'col-sm-3 control-label no-padding-right']) !!}
                        {!! Form::file('image', ['class'=>'col-sm-3 control-label no-padding-right']) !!}
                    </div>

                    @if(isset($slider))
                    <div class="form-group">
                        {!! Form::label('existing_image', 'Existing logo', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            <img src="{{url($slider->image)}}"  height="100" width="100">
                        </div>
                    </div>
                    @endif

                    <div class="form-group">
                        {!! Form::label('links', 'Link', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('links', $slider->links, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Links']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('caption', 'Caption', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('caption', $slider->caption, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Caption']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], null, ['class' => 'col-xs-10 col-sm-5']) !!}
                        </div>
                    </div>


                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Submit
                            </button>&nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection

@section('extra_script_lib')
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
@endsection

@section('extra-scripts')
    <script>
        $('.textarea').ckeditor();
    </script>
    
@endsection