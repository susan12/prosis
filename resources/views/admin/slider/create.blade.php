
@extends('admin.includes.layout')

@section('content')
    <style>
        .adtype {
            display: none;}
    </style>
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            @include($view_path.'.includes.breadcrumb', [
                'panel' => $panel,
                'action' => 'Add Form'
            ])
        </div>

        <div class="page-content">

            @include($view_path.'.includes.breadcrumb_sub', [
                'panel' => $panel,
                'action' => 'Add Form'
            ])

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open([
                        'url' => route($base_route.'.store'),
                        'class' => 'form-horizontal',
                        'role' => 'form',
                        'enctype' => 'multipart/form-data'
                        ]) !!}
                    <div class="form-group">
                        {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('title', null, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Title']) !!}
                        </div>
                    </div>



                    <div class="form-group">
                            {!! Form::label('image', 'Image', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                            <div class="col-sm-9">
                                {!! Form::file('image',['class'=>'col-xs-10 col-sm-5']) !!}
                            </div>
                    </div>
                   
                    <div class="form-group">
                        {!! Form::label('caption', 'Caption', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('caption', null, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Caption']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('links', 'Link', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('links', null, ['class' => 'col-xs-10 col-sm-5', 'placeholder' => 'Title']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], null, ['class' => 'col-xs-10 col-sm-5']) !!}
                        </div>
                    </div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Submit
                            </button>&nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection

@section('extra_script_lib')
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>

@endsection

@section('extra-scripts')
    <script>
        $('.textarea').ckeditor();
    </script>
   
@endsection