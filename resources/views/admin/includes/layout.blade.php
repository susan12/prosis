
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from 198.74.61.72/themes/preview/ace/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 28 Feb 2014 17:44:06 GMT -->

        @include('admin.includes.head')

<body>

        @include('admin.includes.header')

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
            <span class="menu-text"></span>
        </a>

        @include('admin.includes.sidebar')

        @yield('content')

    </div><!-- /.main-container-inner -->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='{{asset('assets/admin-panel/js/jquery-2.0.3.min.js' ) }}'>"+"<"+"/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='{{ asset('assets/admin-panel/js/jquery-1.10.2.min.js') }}'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='{{ asset('assets/admin-panel/js/jquery.mobile.custom.min.js') }}'>"+"<"+"/script>");
</script>
<script src="{{ asset('assets/admin-panel/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin-panel/js/typeahead-bs2.min.js') }}"></script>

<!-- page specific plugin scripts -->

@yield('extra_script_lib')

<script src="{{ asset('assets/admin-panel/js/ace-elements.min.js') }}"></script>
<script src="{{ asset('assets/admin-panel/js/ace.min.js') }}"></script>

<!-- inline scripts related to this page -->

        @yield('extra-scripts')
</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 28 Feb 2014 17:45:06 GMT -->
</html>




