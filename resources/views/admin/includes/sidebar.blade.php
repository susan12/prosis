<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li {!! request()->is('admin/dashboard')?'class="active"':'' !!}>
            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>


        <li {!! request()->is('admin/users*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Users </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/users')?'class="active"':'' !!}>
                    <a href="{{ route('admin.users.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! request()->is('admin/users/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.users.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>

            </ul>
        </li>




        <li {!! request()->is('admin/roles*')?'class="active"':'' !!}>
            <a href="{{ route('admin.roles.index') }}">
                <i class="icon-group"></i>
                <span class="menu-text"> Roles </span>
            </a>
        </li>



        <li {!! request()->is('admin/slider*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-book"></i>
                <span class="menu-text"> Sliders </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/slider')?'class="active"':'' !!}>
                    <a href="{{ route('admin.slider.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! request()->is('admin/slider/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.slider.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>



            </ul>
        </li>

        <li {!! request()->is('admin/ads*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-book"></i>
                <span class="menu-text"> Ads </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/ads')?'class="active"':'' !!}>
                    <a href="{{ route('admin.ads.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! request()->is('admin/ads/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.ads.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>



            </ul>
        </li>
        <li {!! request()->is('admin/blogs*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-book"></i>
                <span class="menu-text"> Blogs </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/blogs')?'class="active"':'' !!}>
                    <a href="{{ route('admin.blogs.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! request()->is('admin/blogs/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.blogs.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>



            </ul>
        </li>

         <li {!! request()->is('admin/video*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-book"></i>
                <span class="menu-text"> Videos </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/video')?'class="active"':'' !!}>
                    <a href="{{ route('admin.video.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! request()->is('admin/video/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.video.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>



            </ul>
        </li>

    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>

</div>
