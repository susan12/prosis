<div class="col-md-offset-3 col-md-9">
    <button class="btn btn-info" type="submit" name="submit">
        <i class="icon-ok bigger-110"></i>
        Submit
    </button>&nbsp;
    <button class="btn btn-info" type="submit" name="submit-continue">
        <i class="icon-ok bigger-110"></i>
        Submit & Continue
    </button>&nbsp;&nbsp; &nbsp;
    <button class="btn" type="reset">
        <i class="icon-undo bigger-110"></i>
        Reset
    </button>
</div>